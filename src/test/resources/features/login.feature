Feature: Login tests

  Background: Be on the login page Background
    Given Be on the login page

    @Positive
  Scenario Outline: Login with valid credentials
    When Put email <email>
    And Put password <password>
    And Click on Submit
    Then User is redirected to his account
      Examples:
        | email                             | password |
        |"danalexandrescu2608@gmail.com" |"bianca"  |
  @Positive
  Scenario Outline: Login with valid credentials and check the message from my account
    When Put email <email>
    And Put password <password>
    And Click on Submit
    Then User is greeted when is redirected to his account
    Examples:
      | email                             | password |
      |"danalexandrescu2608@gmail.com" |"bianca"  |

  @Negative
  Scenario Outline: Login with invalid credentials
    When Put email <email>
    And Put password <password>
    And Click on Submit
    Then User is not redirected to his account
    Examples:
      | email                             | password |
      |"swrongusername.com"               |"bianca"  |

  @Negative
  Scenario Outline: Login with blank credentials
    When Put email <email>
    And Put password <password>
    And Click on Submit
    Then User is not redirected to his account
    Examples:
      | email                             | password |
      |""                                 |""  |