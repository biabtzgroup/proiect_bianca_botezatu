import org.junit.Assert;
import org.junit.Test;
import page_objects.Application;
import page_objects.LoginPageFE;
import utils.BaseClass;

public class NegativeLoginTests extends BaseClass {

    @Test
    public void invalidUsernameTest() {

        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.fillEmail("wrongusername@gmail.com");
        login.fillPassword("bianca");
        login.submit();

        Assert.assertEquals("Autentificare | Neakaisa.ro", driver.getTitle());


    }

    @Test
    public void invalidPasswordTest() {

        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.fillEmail("alexxandrescu.bianca1@gmail.com");
        login.fillPassword("wrongpassword");
        login.submit();

        Assert.assertEquals("Autentificare | Neakaisa.ro", driver.getTitle());
    }

    @Test
    public void invalidUsernameAndPasswordTest() {

        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.fillEmail("wrongusername@gmail.com");
        login.fillPassword("wrongpassword");
        login.submit();

        Assert.assertEquals("Autentificare | Neakaisa.ro", driver.getTitle());


    }

    @Test
    public void blankUsernameAndPasswordTest() {

        LoginPageFE login = new Application(driver).navigateToLoginPage();
        login.fillEmail("");
        login.fillPassword("");
        login.submit();

        Assert.assertEquals("Autentificare | Neakaisa.ro", driver.getTitle());


    }
}

