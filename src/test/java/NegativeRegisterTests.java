import org.junit.Assert;
import org.junit.Test;
import page_objects.Application;
import page_objects.RegisterPage;
import utils.BaseClass;

import java.time.Duration;

public class NegativeRegisterTests extends BaseClass {

    //Verify that if a user tries to register an existing username, an error message is displayed
  /*  @Test
    public void existingAccountErrorOnRegisterTest() throws InterruptedException {
    RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Silvia");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("silvia");
        register.fillConfirmPasswordInput("silvia");
        register.fillCountyInput("Arad");
       // WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        // wait.until(ExpectedConditions.textToBePresentInElement(driver.findElement(By.id("county_idundefined")), "Arad"));
        register.fillCityInput("Aciuta");
        register.fillEmailInput("silviaalexandrescu2503@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro",driver.getTitle());
        //Assert.assertTrue(driver.accountAlreadyExistsAlertIsPresent());
}

    //The following tests check, one by one, if one mandatory field is not filled, the form is not submitted
    @Test
    public void doNotFillSurnameTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillCityInput("Aciuta");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }
    @Test
    public void doNotFillNameTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillCityInput("Aciuta");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }
    @Test
    public void doNotFillPhoneTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Bianca");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillCityInput("Aciuta");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }

    @Test
    public void doNotFillPasswordTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillConfirmPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillCityInput("Aciuta");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }

    @Test
    public void doNotFillConfirmPasswordTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillCityInput("Aciuta");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }
    @Test
    public void doNotFillCountyTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.fillCityInput("Aciuta");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }
    @Test
    public void doNotFillCityTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }
    @Test
    public void doNotFillEmailTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillCityInput("Aciuta");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();

        Assert.assertEquals("Cont nou | Neakaisa.ro", driver.getTitle());
        Assert.assertTrue(register.mandatoryFieldsAreNotFilledAlertIsDisplayed());
    }
    @Test
    public void doNoCheckTermsAndConditionsTest() throws InterruptedException {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Test");
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.fillCountyInput("Arad");
        register.fillCityInput("Aciuta");
        register.fillEmailInput("alexandrescu.bianca100@gmail.com");
        register.registerFormSubmit();

        Assert.assertEquals(driver.switchTo().alert().getText(),"Trebuie sa fiti de acord cu termenii si conditiile.");
    } */
}
