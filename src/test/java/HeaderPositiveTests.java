import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.devtools.v106.page.Page;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.Application;
import page_objects.HeaderPage;
import utils.BaseClass;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import java.time.Duration;

public class HeaderPositiveTests extends BaseClass {
    @Test
    public void clickOnFirstCategoryTest() {
        HeaderPage header = new Application(driver).navigateToHeaderPage();
        Assert.assertEquals("Obiecte sanitare vandute de specialisti | Neakaisa.ro",driver.getTitle());
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(5));
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.cssSelector("a[title=\"Baie\"]"))));
        header.clickOnFirstCategory();
        Assert.assertEquals("Mobilier baie & Obiecte sanitare baie moderne | Neakaisa.ro",driver.getTitle());
    }
    @Test
    public void goToBlogPageTest() {
        HeaderPage header = new Application(driver).navigateToHeaderPage();
        header.goToBlogPageOnCick();
        Assert.assertEquals("https://blog.neakaisa.ro/",driver.getCurrentUrl());
    }
    @Test
    public void goToNewItemsPageTest() {
        HeaderPage header = new Application(driver).navigateToHeaderPage();
        header.goToNewItemsPageOnCick();
        Assert.assertEquals("http://bianca.neakaisa.ro/noutati/",driver.getCurrentUrl());
    }
    @Test
    public void goToContactPageTest() {
        HeaderPage header = new Application(driver).navigateToHeaderPage();
        header.goToContactPageOnCick();
        Assert.assertEquals("http://bianca.neakaisa.ro/contact.htm",driver.getCurrentUrl());
    }

    @Test
    public void goToFavouritesPageTest() {
        HeaderPage header = new Application(driver).navigateToHeaderPage();
        header.goToFavouritesPageOnClick();
        Assert.assertEquals("http://bianca.neakaisa.ro/wishlist/",driver.getCurrentUrl());
    }

}