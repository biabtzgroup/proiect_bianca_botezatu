import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import page_objects.Application;
import page_objects.RegisterPage;
import utils.BaseClass;
import java.time.Duration;


public class PositiveRegisterTests extends BaseClass {

    //Check that, if the user fills all the mandatory fields, the form is submitted and the user is redirected to his account
    @Test
    public void fillInAllMandatoryFieldsTest() throws InterruptedException {


        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.fillSurnameInput("Botezatu");
        register.fillNameInput("Bianca");
        register.fillPhoneInput("0743416144");
        register.fillPasswordInput("bianca");
        register.fillConfirmPasswordInput("bianca");
        register.clickOnCountySearchButton();
        register.fillCountyInput();
//        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(15));
//        wait.until(ExpectedConditions.visibilityOfElementLocated((By.cssSelector("input#city_idundefined"))));
        Thread.sleep(1000);
        register.clickOnCitySearchButton();
        register.fillCityInput();
        register.fillEmailInput("alexandrescu.bianca121@gmail.com");
        register.checkboxTermsAndConditionsCheck();
        register.registerFormSubmit();
        Thread.sleep(10000);

        Assert.assertEquals("Contul meu | Neakaisa.ro",driver.getTitle());
    }

    //Check that, if you click on terms and conditions link, a modal with the terms is displayed
    @Test
    public void checkTheTermsAndConditionsLinkTest() {
        RegisterPage register = new Application(driver).navigateToRegisterPage();
        register.linkTermsAndConditionsClick();
        Assert.assertTrue(register.termsAndConditionsModalCloseButtonIsPresent());
    }
}
