package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HeaderPage {
    private WebDriver driver;
    private By firstCategory = By.cssSelector("a[title=\"Baie\"]");

    private By blogButton = By.cssSelector("a[href=\"https://blog.neakaisa.ro/\"]");
    private By newItemsButton = By.cssSelector("a[class=\"nav-link underline-noutati\"]");
    private By contactPageButton = By.cssSelector("a[class=\"nav-link underline-contact\"]");

    private By favouritesIcon = By.cssSelector("i[class=\"n-icon-Favorite\"]");

    public HeaderPage(WebDriver driver){
        this.driver=driver;
    }

    public void clickOnFirstCategory(){
        driver.findElement(firstCategory).click();
    }
    public void goToBlogPageOnCick(){
        driver.findElement(blogButton).click();
    }

    public void goToNewItemsPageOnCick(){ driver.findElement(newItemsButton).click(); }

    public void goToContactPageOnCick(){ driver.findElement(contactPageButton).click(); }
    public void goToFavouritesPageOnClick(){
        driver.findElement(favouritesIcon).click();
    }
}
