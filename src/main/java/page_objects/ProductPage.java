package page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ProductPage {

        private WebDriver driver;
        private By lastBreadcrumbLink = By.cssSelector("a[href=\"http://bianca.neakaisa.ro/capace-wc/filtre/producator/ideal-standard/colectie/tesi/\"]");

        private By addToWishlistFromProductPageButton = By.cssSelector("button#product-to-wishlist");

        private By wishlistLink = By.cssSelector("i.n-icon-Favorite");
        private By removeProductFromWishlistPage = By.cssSelector("button[class=\"p-box-remove-from-wishlist\"]");
        private By addToCartButton = By.cssSelector("button.add-to-basket-btn");
        private By viewCartButton = By.xpath("//body/div[@id='added_product']/div[@id='ap-box-product']/a[1]");
        private By viewProductDescriptionButton = By.xpath("//*[@id=\"product-view-wrapper\"]/div[3]/div[1]/div[1]/div/ul/li[1]/a");
        private By viewProductAttributesButton = By.xpath("//*[@id=\"product-view-wrapper\"]/div[3]/div[1]/div[1]/div/ul/li[2]/a");
        private By viewProductBrandButton = By.xpath("//*[@id=\"product-view-wrapper\"]/div[3]/div[1]/div[1]/div/ul/li[3]/a");
        private By viewProductReviewsButton = By.xpath("//*[@id=\"product-view-wrapper\"]/div[3]/div[1]/div[1]/div/ul/li[4]/a");

        private By addProductReviewButton = By.cssSelector("button.review-btn");

        private By addProductReviewModal = By.cssSelector("button[type=\"submit\"][class=\"btn btn-reversed\"]");

    public ProductPage(WebDriver driver) {
        this.driver=driver;
    }
    public void lastBreadcrumbClick(){
        driver.findElement(lastBreadcrumbLink).click();
    }
    public void addProductToWishlistOnClick(){
        driver.findElement(addToWishlistFromProductPageButton).click();
    }
    public void redirectToWishlistOnClick(){
        driver.findElement(wishlistLink).click();
    }
    public boolean checkIfRemoveProductFromWishlistButtonIsDisplayed() {
       return driver.findElement(removeProductFromWishlistPage).isEnabled();
    }
    public void addProductToCartOnClick(){
        driver.findElement(addToCartButton).click();
    }
    public void viewCartOnClick(){
        driver.findElement(viewCartButton).click();
    }
    public void viewProductDescriptionOnClick(){
        driver.findElement(viewProductDescriptionButton).click();
    }
    public void viewProductAttributesOnClick(){
        driver.findElement(viewProductAttributesButton).click();
    }
    public void viewProductBrandDescriptionOnClick(){
        driver.findElement(viewProductBrandButton).click();
    }
    public void viewProductReviewsOnClick(){
        driver.findElement(viewProductReviewsButton).click();
    }
    public void addProductReviewOnClick(){
        driver.findElement(addProductReviewButton).click();
    }
    public boolean checkIfAddProductReviewModalIsOpenedOnClick(){
        return driver.findElement(addProductReviewModal).isEnabled();
    }

}
